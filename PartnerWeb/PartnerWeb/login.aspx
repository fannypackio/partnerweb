﻿<%@ Page Language="C#" Inherits="PartnerWeb.login" MasterPageFile="~/Site.master"%>


<asp:Content ID="PageContent" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
<!-- for partner login page -->
    <div class="section-login-partner section-white">
        <div class="container">
            <div class="row row-cover row-features row-flex row-flex-center">
                <div class="col-md-4">
                    <div class="component text-left block-inputs">
                        <h3>Partner Login</h3>
                        <h6>Log in to your Partner Admin account</h6>
                        <form runat="server">
                            <input class="text-field" placeholder="Email Address" type="text" id="txt_email" runat="server"/>
                            <input class="text-field" placeholder="Password" type="password" id="txt_password" runat="server"/>
                           <!-- <button type="submit" class="cta cta-main cta-submit">Submit</button> -->
                            <asp:Button CssClass="cta cta-main cta-submit" runat="server" id="Btn_Login" OnClick="Btn_Login_Click" Text="Submit"></asp:Button>
                            <h6 class="inherit-font-size text-center">Lose access to your Partner Admin account? <a href="mailto:help@hellojumper.com?subject=Partner%20Admin%20Access%20Help">Contact Us</a></h6>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        
    </script>
</asp:Content>

    