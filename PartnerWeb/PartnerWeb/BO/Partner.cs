﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartnerWeb
{
    public class Partner
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postal { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string TimeZone { get; set; }
        public string Loc_LAT_centroid { get; set; }
        public string Loc_LAT_poly { get; set; }
        public string Loc_LONG_centroid { get; set; }
        public string loc_LONG_poly { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public byte IsClaimed { get; set; }
        public string InviteCode { get; set; }
        public string ProfilePhoto { get; set; }
        public string CoverPhoto { get; set; }
        public string ProfilePhotoURL
        {
            get
            {
                return "https://d15bqe4cdm89bh.cloudfront.net/fit-in/" + this.ProfilePhoto;
            }
        }
        public string CoverPhotoURL
        {
            get
            {
                return "https://d15bqe4cdm89bh.cloudfront.net/fit-in/" + this.CoverPhoto;
            }
        }
        public int IsActiveCreditCard { get; set; }
    }
}