﻿using System;
namespace PartnerWeb
{
	public class OperatingHours
    {
        public Guid ID { get; set; }
        public int PartnerID { get; set; }
        public Guid UserID { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int UTCStartTime { get; set; }
        public int UTCEndTime { get; set; }
        public int Day { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
    public class InviteTeam
    {
        public string List { get;set;}
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
