using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;
namespace PartnerWeb
{
    public partial class Site : System.Web.UI.MasterPage
    {

        protected string _userName = "";
        protected string _userEmail = "";
        protected string _partnerName = "";
        public User _user = null;
        public Partner _partner = null;

        public void Page_Load(object source, EventArgs e)
        {
            lnkIcon.NavigateUrl = "//hellojumper.com/partner";
            HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
            if (userCookie != null && !string.IsNullOrEmpty(userCookie.Value))
            {
                lnkIcon.NavigateUrl = "//partner.hellojumper.com/team";
               
                //Load UserDetails
                //Load PartnerDetails
                getUserDetails();
                if(_user!=null && _user.ID != Guid.Empty)
                {
                    getPartnerDetails(_user.PartnerID);
                    if (_partner != null)
                    {
                        _partnerName = _partner.Name;
                    }
                }

            }
        }

        public void setPageTitle(String Title)

        {
			pageTitle.Text = Title;
        }
		public void hideHeaderLinks(){
			pillnav.Visible = false;
			headerlinks.Visible = false;
		}
        public void hidePartnerName() {
            partnerNameDiv.Visible = false;
        }
		public void showError(String message){

			errorWrap.Attributes["class"] += " active";
			errorLabel.InnerText = message;
        }
        public void showContinueError(String message,string linkMessage="", string link="", Boolean success = false)
        {
            errorWrap.Attributes["class"] += " active";
            if (success) { errorWrap.Attributes["class"] += " success"; }
            errorLabel.InnerText = message;
            if (linkMessage != "")
            {
                continueLinkText.Text = linkMessage;
                continueLink.HRef = link;
                continueLink.Visible = true;
            }
        }
        public void setPageDesc(String Desc) {
            pageDesc.Attributes["content"] = Desc;
            pageDescAlt.Attributes["content"] = Desc;
            pageDescFb.Attributes["content"] = Desc;
            pageDescTwt.Attributes["content"] = Desc;
        }
        public void setActiveHeaderLink(String pageName) {
            if (pageName == "manage") {
                managebusinesslink.Attributes["class"] += (" " + "cta-underline");
            } else if (pageName == "plans") {
                planslink.Attributes["class"] += (" " + "cta-underline");
            } else {
                
            }
        }
        /*
        public void setUserDetails(PartnerWeb.User _user)
        {
            _userName = _user.FirstName + " " + _user.LastName;
            _userEmail = _user.Email;
        }
        public void setPartnerDetails(PartnerWeb.Partner _partner)
        {
            _partnerName = _partner.Name;
        }
        */
        public User getUserDetails()
        {
            if (_user == null)
            {
                HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
                if (userCookie != null && !string.IsNullOrEmpty(userCookie.Value))
                {
                    string access_token = userCookie.Value;
                    _user = PartnerWeb.User.GetUserInfo(access_token);
                    _userName = _user.FirstName + " " + _user.LastName;
                    _userEmail = _user.Email;
                }
            }
            return _user;
        }
        public Partner getPartnerDetails(int partnerid)
        {
            if (_partner == null)
            {
                if (partnerid > 0)
                {
                    string url = ConfigurationManager.AppSettings["JumperCustomer.API.Url"] + "/Partners/ID/" + partnerid;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = "GET";
                    //request.Headers.Add("Authorization", "bearer " + token);
                    request.ContentType = "application/json";
                    HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
                    string responseText;

                    using (var webresponse = request.GetResponse())
                    {
                        using (var reader = new StreamReader(webresponse.GetResponseStream()))
                        {
                            responseText = reader.ReadToEnd();
                            Partner oPartner = JsonConvert.DeserializeObject<Partner>(responseText);
                            if (oPartner != null)
                            {
                                //Partner oPartner = oPartners[0];
                                //litInviteCode.Text = oPartner.InviteCode;
                                //hdn_PartnerInviteCode.Value = oPartner.InviteCode;
                                //hdn_PartnerName.Value = oPartner.Name;
                                //((Site)Master).setPartnerDetails(oPartner);
                                _partner = oPartner;
                                _partnerName = _partner.Name;
                                if(oPartner.IsActiveCreditCard == 0)
                                {
                                    this.showContinueError("Payment failed, Please check your credit card details", "Click here", "/pricing");
                                }
                            }
                            else
                            {
                                //blank look
                            }
                        }
                    }
                }
                else
                {
                    //User not Connected to business
                }
            }
            return _partner;
        }
    }
}
