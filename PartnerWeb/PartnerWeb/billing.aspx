﻿<%@ Page Language="C#" Inherits="PartnerWeb.plans_and_billing" MasterPageFile="~/Site.master" CodeBehind="~/billing.aspx.cs" %>

<asp:Content ID="PageContent" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    
    <script src="https://js.stripe.com/v3/"></script>
    <style type="text/css">
        .StripeElement {
          background-color: white;
          height: 40px;
          padding: 10px 12px;
          border-radius: 4px;
          border: 1px solid transparent;
          box-shadow: 0 1px 3px 0 #e6ebf1;
          -webkit-transition: box-shadow 150ms ease;
          transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
          box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
          border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
          background-color: #fefde5 !important;
        }

    </style>

    <div class="section-admin-manage-business section-white-pure no-flex-add-padding">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="component component-v-spacing text-center">
                        <a href="/pricing" id="ComponentPlansLink" class="cta admin-component-link">Pricing</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="component component-v-spacing text-center">
                        <a href="/billing" id="ComponentBillingLink" class="cta cta-underline admin-component-link">Billing</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="admin-component active admin-component-billing">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="component component-billing-summary">
<!--                                    <h3>Current Plan: <asp:Literal ID="litPlanName" runat="server"></asp:Literal></h3>-->
                                </div>
                            </div>
                            <form action="/billing" method="post" id="billingform" runat="server">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="component">
                                                <h6>Current Payment Method: <span class="payment-active"><asp:Literal ID="litCurrentCard" runat="server"></asp:Literal></span></h6>
                                                <a id="btn_addcoupon" class="cta cta-main">Add/Change Method</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="component component-new-cc block-inputs">
                                                <div class="form-row">
                                                    <label for="card-element">
                                                        Credit or debit card
                                                    </label>
                                                    <div id="card-element">
                                                        <!-- A Stripe Element will be inserted here. -->
                                                    </div>

                                                    <!-- Used to display form errors. -->
                                                    <div id="card-errors" role="alert"></div>
                                                </div>

                                                <button id="btn_submitcard" class="cta cta-margin-20 cta-main cta-green" type="submit">Submit</button>
                                                <a class="cta cta-cancel cta-cancel-method">Cancel</a>
                                                <asp:HiddenField id="hdn_stripetoken" runat="server"/>
                                                <a class="cta cta-red" style="display:none;">Remove & Downgrade</a>
                                                <%--
                                                <input class="text-field border-field" type="text" name="" value="">
                                                <input class="text-field border-field" type="text" name="" value="">
                                                <input class="text-field border-field" type="text" name="" value="">
                                                <button id="UpdateCardButton" class="cta cta-main">Update Card</button>
                                                <button class="cta cta-cancel-cc">Cancel</button> --%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="component component-promo-code block-inputs">
                                                <h6>Current Promo Code: <span class="promo-code-value"><asp:Literal ID="litAddedCoupon" runat="server">None</asp:Literal></span></h6>
                                                <h6 id="msg_coupondisabled" runat="server" visible="true">Add payment card to enable promo code</h6>
                                                <a id="ChangePromoButton" runat="server" visible="false" class="cta cta-main">Add/Change Code</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="component component-new-promo block-inputs">
                                                <input placeholder="Enter promo code" class="text-field border-field" type="text" name="" value="" id="txt_coupon" runat="server">
                                                <button id="btn_submitcoupon" class="cta cta-main" type="submit"">Submit</button>
                                                <a class="cta cta-cancel-promo">Cancel</a>
                                            </div>                             
                                        </div>
                                    </div>
                                </div>
                            </form>                         
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="component component-totals">
                                    <h5>Current Billing Information</h5>
                                    <h6>Total Team Members:
                                        <span class="total-amt"><asp:Literal ID="litTeamCount" runat="server"></asp:Literal></span>
                                    </h6>
                                    <h6>Price Per Team Member:
                                        <%--<span class="total-amt price-old">$19</span>--%>
                                        <span id="spnPlanPrice" runat="server" class="total-amt price-discount"><asp:Literal ID="litPlanPrice" runat="server"></asp:Literal></span>
                                        <span id="spnDiscountedPlanPrice" runat="server" class="total-amt price-discount" visible="false"><asp:Literal ID="litDiscountedPlanPrice" runat="server"></asp:Literal></span>
                                        <span>/month</span>
                                    </h6>
                                    <h6>Total:
                                        <span id="spnTotalAmount" runat="server"  class="total-amt"><asp:Literal ID="litTotalAmount" runat="server"></asp:Literal></span>
                                        <span id="spnDiscountedTotalAmount" runat="server"  class="total-amt" visible="false"><asp:Literal ID="litDiscountedTotalAmount" runat="server"></asp:Literal></span>
                                        <span>/month</span>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">// Create a Stripe client.
        var stripe = Stripe('<%= ConfigurationManager.AppSettings["Stripe.API.PublishableKey"] %>');


        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                lineHeight: '18px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', { style: style });

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function (event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        //creditcard submit button
        $("#btn_submitcard").click(function () {
            event.preventDefault();

            stripe.createToken(card).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        //coupon code submit button
        $("#btn_submitcoupon").click(function () {
            event.preventDefault();
            var coupon = $("[id$=txt_coupon]").val();
            if (coupon.trim() != "") {
                // Submit the form
                form.submit();
            }
        });



        // Handle stripe form submission.
        var form = document.getElementById('billingform');
       /* form.addEventListener('submit', function (event) {
            
        });
        */
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            /*var form = document.getElementById('billingform');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);
            */
            $("input[id$=hdn_stripetoken]").val(token.id);
            // Submit the form
            form.submit();
        }

    </script>
</asp:Content>