﻿using System;
using System.Web;
using System.Web.UI;

namespace PartnerWeb
{

    public partial class signout : System.Web.UI.Page
    {
		public void Page_Load(object source, EventArgs e)
        {
            if (!IsPostBack)
            {
                string cookieName = "PartnerCookie";
                if (Request.Cookies[cookieName] != null)
                {
                    var myCookie = new HttpCookie(cookieName);
                    myCookie.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie);
                }
                //Response.Cookies.Remove("PartnerCookie");
				Response.Redirect("login?ref=so");
            }
        }
    }
}
