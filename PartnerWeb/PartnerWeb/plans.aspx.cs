﻿using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;

namespace PartnerWeb
{

    public partial class plans : System.Web.UI.Page
    {

        private PartnerWeb.User _user = null;
        private StripeCard _CurrentCard = null;
        PartnerBilling _PartnerBilling = null;

        protected override void OnPreRender(EventArgs e) {
            ((Site)Master).setPageTitle("Pricing");
            ((Site)Master).setPageDesc("Jumper Partner Pricing");
            ((Site)Master).setActiveHeaderLink("plans");
            base.OnPreRender(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            String access_token = "";
            HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
            if (userCookie == null)
            {
                Response.Redirect("login");
            }
            else
            {
                access_token = userCookie.Value;
                getUserDetails(access_token);

            }

            if (_PartnerBilling != null)
            {/*
                if (_PartnerBilling.PaymentPlanID == ConfigurationManager.AppSettings["Stripe.Product.Free"])
                {//Individual
                    PlanIndividual.Attributes["class"] += " selected";
                }
                else if (_PartnerBilling.PaymentPlanID == ConfigurationManager.AppSettings["Stripe.Product.Paid"])
                {//Team
                    PlanSmall.Attributes["class"] += " selected";
                }
                else
                {//Enterprise
                    PlanEnterprise.Attributes["class"] += " selected";
                }

                if (_CurrentCard != null && !string.IsNullOrEmpty(_CurrentCard.Id))
                {
                    //litCurrentCard.Text = _CurrentCard.Brand + " card ending with " + _CurrentCard.Last4;
                }
                else
                {
                    //litCurrentCard.Text = "None";
                }*/
            }
        }
        private void getUserDetails(string token)
        {
            _user = ((Site)Master).getUserDetails();
            getPartnerBilling(_user.PartnerID, token);

            if (_PartnerBilling != null)
            {
                StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["Stripe.API.SecretKey"]);

                var customerService = new StripeCustomerService();
                StripeCustomer customer = customerService.Get(_PartnerBilling.PaymentCustomerID);//"cus_DMadQNAyKCGdnq"
                //Credit Card
                if (customer.Sources != null && customer.Sources.TotalCount > 0)
                {
                    _CurrentCard = customer.Sources.Data[0].Card;

                }
                if(_CurrentCard==null)
                {
                    //Show button
                    btn_billing.Visible = true;
                }
                //Discount
                if (customer.StripeDiscount != null)
                {

                }
                //StripeSubscription _FreeSubscription = null;
                //StripeSubscription _PaidSubscription = null;
                //Products,Plans,Subscriptions
                if (customer.Subscriptions != null && customer.Subscriptions.TotalCount > 0)
                {
                    foreach (StripeSubscription ss in customer.Subscriptions)
                    {
                        if(ss.StripePlan.Id == _PartnerBilling.PaymentPlanID)
                        {
                            if (_PartnerBilling.PaymentPlanID == ConfigurationManager.AppSettings["Stripe.Plan.Free"])
                            {//Individual
                                PlanIndividual.Attributes["class"] += " selected";
                                //litPlanName.Text = ss.StripePlan.Nickname;
                            }
                            else if (_PartnerBilling.PaymentPlanID == ConfigurationManager.AppSettings["Stripe.Plan.Paid"])
                            {//Team
                                PlanSmall.Attributes["class"] += " selected";
                                //litPlanName.Text = ss.StripePlan.Nickname;
                            }
                            else
                            {//Enterprise
                                PlanEnterprise.Attributes["class"] += " selected";
                                //litPlanName.Text = "Enterprise";
                            }
                            break;
                        }
                    }
                }
                //if (string.IsNullOrEmpty(litPlanName.Text))
                //{
                //    litPlanName.Text = "Individual";
                //    PlanIndividual.Attributes["class"] += " selected";
                //}

            }
        }
        private void getPartnerBilling(int partnerid, string token)
        {

            string url = ConfigurationManager.AppSettings["JumperPartner.API.Url"] + "/Account/GetPartnerBilling?partnerID=" + partnerid;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add("Authorization", "bearer " + token);
            request.ContentType = "application/json";
            HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
            string responseText;

            using (var webresponse = request.GetResponse())
            {
                using (var reader = new StreamReader(webresponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    List<PartnerBilling> ListPartnerBilling = JsonConvert.DeserializeObject<List<PartnerBilling>>(responseText);
                    if (ListPartnerBilling.Count > 0)
                    {
                        _PartnerBilling = ListPartnerBilling[0];
                    }
                }
            }
        }
    }
}
