﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Web.Configuration;
using Stripe;
using System.Configuration;

namespace PartnerWeb.Members
{
    public partial class Teams : System.Web.UI.Page
    {
        private string access_token = string.Empty;
        private List<User> oEmployeeUsers = null;
        protected override void OnPreRender(EventArgs e)
        {
            ((Site)Master).setPageTitle("Jumper Team");
            ((Site)Master).setActiveHeaderLink("manage");
            ((Site)Master).setPageDesc("Manage your team on Jumper");
            base.OnPreRender(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
                if (userCookie == null)
                {
                    Response.Redirect("login");
                }
                else
                {
                    inviteBlock.Visible = false;
                    billingBlock.Visible = true;
                    access_token = userCookie.Value;
                    getUserDetails(access_token);

                }
            }
        }
        private void getUserDetails(string token)
        {
            User _user = ((Site)Master).getUserDetails();
            if (_user != null)
            {
                hdn_PartnerID.Value = _user.PartnerID.ToString();
                getPartnerDetails(_user.PartnerID);
                getTeamMembers(_user.PartnerID);
                
                getPartnerBilling(_user.PartnerID, token);
            }
            
        }
        private void getPartnerDetails(int partnerid)
        {
            if (partnerid > 0)
            {
                Partner oPartner = ((Site)Master).getPartnerDetails(partnerid);
                if (oPartner != null)
                {
                    //Partner oPartner = oPartners[0];
                    litInviteCode.Text = oPartner.InviteCode;
                    hdn_PartnerInviteCode.Value = oPartner.InviteCode;
                    hdn_PartnerName.Value = oPartner.Name;
                }
                else
                {
                    //blank look
                }
            }
            else
            {
                //User not Connected to business
            }
        }
        private void getPartnerBilling(int partnerid,string token)
        {
            PartnerBilling _PartnerBilling = null;
            string url = ConfigurationManager.AppSettings["JumperPartner.API.Url"] + "/Account/GetPartnerBilling?partnerID=" + partnerid;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add("Authorization", "bearer " + token);
            request.ContentType = "application/json";
            HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
            string responseText;

            using (var webresponse = request.GetResponse())
            {
                using (var reader = new StreamReader(webresponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    List<PartnerBilling> ListPartnerBilling = JsonConvert.DeserializeObject<List<PartnerBilling>>(responseText);
                    if (ListPartnerBilling.Count > 0)
                    {
                        _PartnerBilling = ListPartnerBilling[0];
                        if (_PartnerBilling != null)
                        {
                            StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["Stripe.API.SecretKey"]);

                            var customerService = new StripeCustomerService();
                            StripeCustomer customer = customerService.Get(_PartnerBilling.PaymentCustomerID);
                            if (customer.Sources != null && customer.Sources.TotalCount > 0)
                            {
                                StripeCard card = customer.Sources.Data[0].Card;
                                if (card != null)
                                {
                                    inviteBlock.Visible = true;
                                    billingBlock.Visible = false;
                                }
                                else
                                {
                                    inviteBlock.Visible = false;
                                    billingBlock.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            //return _CustomerInfo;
        }

        /*
        private void getPartnerBillingCards(int partnerid, string token)
        {
            StripeCard _PartnerBillingCard = null;
            string url = "http://partnerapi.techscotch.com/api/Account/GetPartnerBillingCardDetails";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add("Authorization", "bearer " + token);
            request.ContentType = "application/json";
            HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
            string responseText;

            using (var webresponse = request.GetResponse())
            {
                using (var reader = new StreamReader(webresponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    List<StripeCard> ListPartnerBillingCards = JsonConvert.DeserializeObject<List<StripeCard>>(responseText);
                    if (ListPartnerBillingCards.Count > 0)
                    {
                        _PartnerBillingCard = ListPartnerBillingCards[0];
                        inviteBlock.Visible = true;
                        billingBlock.Visible = false;
                    }
                }
            }
            //return _CustomerInfo;
        }
        */
        private void getTeamMembers(int partnerid)
        {
            if (partnerid > 0)
            {
                string url = ConfigurationManager.AppSettings["JumperPartner.API.Url"] + "/account/GetMembers";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.Headers.Add("Authorization", "bearer " + access_token);
                request.ContentType = "application/json";
                HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
                string responseText;

                using (var webresponse = request.GetResponse())
                {
                    using (var reader = new StreamReader(webresponse.GetResponseStream()))
                    {
                        responseText = reader.ReadToEnd();
                        oEmployeeUsers = JsonConvert.DeserializeObject<List<User>>(responseText);
                        if (oEmployeeUsers != null && oEmployeeUsers.Count > 0)
                        {
                            rpt_teammembers.DataSource = oEmployeeUsers;
                            rpt_teammembers.DataBind();
                        }
                        else
                        {
                            //blank look
                        }
                    }
                }
            }
        }
        public void rpt_teammembers_ItemCreated(Object Sender, RepeaterItemEventArgs e)
        {
            /*
            HtmlSelect daySelect = (HtmlSelect)e.Item.FindControl("daySelect");
            HtmlInputGenericControl starttime = (HtmlInputGenericControl)e.Item.FindControl("starttime");
            HtmlInputGenericControl endtime = (HtmlInputGenericControl)e.Item.FindControl("endtime");
            //DropDownList asd = (DropDownList)e.Item.FindControl("asd");
            //asd.Items.Add(new ListItem("SUnday", "0"));
            daySelect.SelectedIndex = daySelect.Items.IndexOf(daySelect.Items.FindByValue(listHours[e.Item.ItemIndex].Day.ToString()));
            TimeSpan startspan = TimeSpan.FromMinutes(listHours[e.Item.ItemIndex].StartTime);
            TimeSpan endspan = TimeSpan.FromMinutes(listHours[e.Item.ItemIndex].EndTime);
            starttime.Value = startspan.ToString(@"hh\:mm\:ss");
            endtime.Value = endspan.ToString(@"hh\:mm\:ss");
            //e.Item.ItemIndex*/
            Label lblName = (Label)e.Item.FindControl("lblName");
            Label lblEmail = (Label)e.Item.FindControl("lblEmail");
            HtmlAnchor empAvailabilityBtn = (HtmlAnchor)e.Item.FindControl("empAvailabilityBtn");
            HtmlAnchor empRemoveBtn = (HtmlAnchor)e.Item.FindControl("empRemoveBtn");
            HtmlImage empPhoto = (HtmlImage)e.Item.FindControl("empPhoto");

            lblName.Text = oEmployeeUsers[e.Item.ItemIndex].FirstName + " " + oEmployeeUsers[e.Item.ItemIndex].LastName;
            lblEmail.Text = oEmployeeUsers[e.Item.ItemIndex].Email;

            if (!string.IsNullOrEmpty(oEmployeeUsers[e.Item.ItemIndex].ImageURL))
            {
                empPhoto.Src = oEmployeeUsers[e.Item.ItemIndex].ImageURL;
            }
            else
            {
                empPhoto.Src = WebConfigurationManager.AppSettings["webstatic"] + "/img/default-profile.png";
            }

            if(oEmployeeUsers[e.Item.ItemIndex].IsAvailable == 1)
            {
                empAvailabilityBtn.InnerText = "Make Unavailable";
                empAvailabilityBtn.Attributes["class"] += " cta-red";
            }
            else
            {
                empAvailabilityBtn.InnerText = "Make Available";
                empAvailabilityBtn.Attributes["class"] += " cta-green-text";
            }
            if (oEmployeeUsers[e.Item.ItemIndex].IsDeleted == 1 || oEmployeeUsers[e.Item.ItemIndex].IsAdmin == 1)
            {
                empRemoveBtn.Visible = false;
            }
            empAvailabilityBtn.Attributes.Add("data-empid", oEmployeeUsers[e.Item.ItemIndex].ID.ToString());
            empRemoveBtn.Attributes.Add("data-empid", oEmployeeUsers[e.Item.ItemIndex].ID.ToString());
            empAvailabilityBtn.Attributes.Add("data-avialable", oEmployeeUsers[e.Item.ItemIndex].IsAvailable.ToString());
            empRemoveBtn.Attributes.Add("data-deleted", oEmployeeUsers[e.Item.ItemIndex].IsDeleted.ToString());
        }
    }
}