﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="photos.aspx.cs" Inherits="PartnerWeb.Members.photos"  MasterPageFile="~/Site.master"%>

<asp:Content id="PageJSContent" ContentPlaceHolderID="contentJSPlaceHolder" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["webstatic"] %>/js/partner/photo.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["webstatic"] %>/js/lib/simpleupload.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["webstatic"] %>/js/lib/jquery.cropit.js"></script>
    <style>
      .profilephoto .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
      }
      .coverphoto .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 806px;
        height: 250px;
      }
      .cropit-preview-background {
          opacity: .2;
        }

      .cropit-preview-image-container {
        cursor: move;
      }
      .image-size-label {
        margin-top: 10px;
      }
      input, .export {
        display: block;
      }
      button {
        margin-top: 10px;
      }
    </style>
    <script>
      $(function() {
          $('.image-editor.profilephoto').cropit({
              imageState: {
                  src: '<%= profilePhotoURL %>'
                  //src: 'https://s3.us-west-2.amazonaws.com/testjumper/3b0105f8-19bd-4675-b473-1e084c3f7969?response-content-disposition=inline&X-Amz-Security-Token=AgoGb3JpZ2luEG0aCXVzLXdlc3QtMSKAAngP1fQQaToVfxcf7CEo8ZWb39%2Frbg18hXQVYVKaJc7bjAjv4z9QWnhPjCgv3izhwV3VuhoBPUDWBFGV01k4kqReBKbTbhTg4%2FaI6oQA5YWYn3PkQ2wkwzIuWsBPcIAITu%2BpvLT4U407RWtJ7fT9TVKt1SpVbRobyNZG3G34BfdQFhRhe1tZ%2BcWqLlONIExQ0K9hR3oQFd3C1axi78pedIHhHGjl5C%2BCjcOFZM06xjRJgNmhYlyy9TDIptiqSSe2Uk5AjD2XQHRvsUvRbyIwoL9doy8GeR5rx0rodB4pD4JrsAHFqexdGet6Ir1N0OUdAIYzyGT4j31MQ4r%2B2%2Fi9%2FJwqpgII8%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgw2MjUxODUyMjUzNzQiDNLBJH%2FrDHi2kTTktir6AWLqaCi04xzpxSMQ76uID7oAqMme%2BeBb92VAm425rctjHSxXyxUECRAGhjafEDDpn05w1i8XG67OCYaz23Z6oUHbYW7lhPopV%2FLw3XJH3PoSVcUbNnk9u%2BScXyXaGYDbghqk3mBNCux%2FZU1VzzDRh72P3Gm032tlfCaY5MomYifASUmlLDLhf1hMVu2YMsqc9uUgDIklFSrVnDmzYfBUz3S9uDD%2B1i1pWXyvNDM2pnTHG7GSgDfgtnDq%2FSWS3uN8Nr%2Feq%2FXOJu6sf%2BKEs9zTqi5R5WyCyhe%2F5J8pBsJi3ppBkm%2BdvJhaNBol%2B9rkM0F%2B%2F4aCKlXKctpCfoowhaHw2AU%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20180610T011749Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIAJTWD5QR4VPCKWW7A%2F20180610%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=c0a2d5378345e7b645cd15d068facf0964c89dc600d43d65e02cbc85f243b3f8'
              },
              imageBackground: true,
              'smallImage': 'allow',
              'maxZoom': 5
          });
          $('.image-editor.profilephoto').cropit('zoom', 1.75);
          $('.image-editor.coverphoto').cropit({
              imageState: {
                  src: '<%= coverPhotoURL %>'
              },
              imageBackground: true,
              'smallImage': 'allow',
              'maxZoom': 5        
        });
        $('.rotate-cw').click(function() {
          $('.image-editor').cropit('rotateCW');
        });
        $('.rotate-ccw').click(function() {
          $('.image-editor').cropit('rotateCCW');
        });
          
        $('.image-editor.profilephoto .export').click(function () {
            var imageData = $('.image-editor.profilephoto').cropit('export');
          var fd = new FormData();
          var files = imageData;
          fd.append('file', files);
          $.ajax({
              url: '/ajax/uploadprofile.ashx?partnerid=<%=gpartnerid%>',
              type: 'post',
              dataType: 'json',
              data: fd,//data: { base64data: imageData },
              contentType: false,
              processData: false,
              success: function (response) {
                  showSuccessNotification("Profile Photo Uploaded");
                  $(".business-photo").attr("src", response.url);
                  $(".btnCancelChangeProfilePic").click();
              },
              failure: function (response) {

              }
          });
        });
        $('.image-editor.coverphoto .export').click(function () {
            var imageData = $('.image-editor.coverphoto').cropit('export');
          var fd = new FormData();
          var files = imageData;
          fd.append('file', files);
          $.ajax({
              url: '/ajax/uploadcover.ashx?partnerid=<%=gpartnerid%>',
              type: 'post',
              dataType: 'json',
              data: fd,//data: { base64data: imageData },
              contentType: false,
              processData: false,
              success: function (response) {
                  showSuccessNotification("Cover Photo Uploaded");
                  $(".cover-photo").attr("src", response.url);
                  $(".btnCancelChangeCoverPic").click();
              },
              failure: function (response) {

              }
          });
        });
          $(".btnChangeProfilePic").click(function () {
              $(".profilephoto.edited-mode").hide();
              $(this).hide();
              $(".profilephoto.edit-mode").show();
              $(".btnCancelChangeProfilePic").show();
          });
          $(".btnCancelChangeProfilePic").click(function () {
              $(".profilephoto.edited-mode").show();
              $(".btnChangeProfilePic").show();
              $(".profilephoto.edit-mode").hide();
              $(".btnCancelChangeProfilePic").hide();
          });
          $(".btnChangeCoverPic").click(function () {
              $(".coverphoto.edited-mode").hide();
              $(this).hide();
              $(".coverphoto.edit-mode").show();
              $(".btnCancelChangeCoverPic").show();
          });
          $(".btnCancelChangeCoverPic").click(function () {
              $(".coverphoto.edited-mode").show();
              $(".btnChangeCoverPic").show();
              $(".coverphoto.edit-mode").hide();
              $(".btnCancelChangeCoverPic").hide();
          });
      });
    </script>
</asp:Content>
<asp:Content ID="PageContent" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="section-admin-manage-business section-white-pure no-flex-add-padding">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentTeamLink" href="/team" class="cta admin-component-link">Team</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentPhotosLink" class="cta admin-component-link cta-underline">Photos</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentHoursLink" href="/hours" class="cta admin-component-link">Hours</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="admin-component admin-component-photos active">
                        <div class="col-md-6">
                            <div class="component">
                                <button class="cta cta-main cta-inline btnChangeProfilePic" id="btn_updateprofilepic">Change/Edit Photo</button>
                                <button class="cta cta-main cta-inline cta-photo-cancel btnCancelChangeProfilePic" style="display:none;">Cancel</button>
                            </div>
                            <div class="component component-v-spacing-sm component-style component-business-photo text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-12">
                                        <h5>Default Pic</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="business-photo-wrapper edited-mode profilephoto">
                                            <img class="business-photo" id="profilepic" runat="server" src="/img/default-business.png"/>
                                        </div>
                                        <div class="image-editor profilephoto edit-mode" style="display:none;">
                                            <input type="file" class="cropit-image-input">
                                            <div class="cropit-preview"></div>
                                            <div class="image-size-label">
                                            Resize image
                                            </div>
                                            <input type="range" class="cropit-image-zoom-input">
                                            <button class="rotate-ccw">Rotate counterclockwise</button>
                                            <button class="rotate-cw">Rotate clockwise</button>

                                            <button class="export">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="component">
                                <button class="cta cta-main cta-inline cta-top-spread-mobile  btnChangeCoverPic" id="btn_updatecoverpic">Change/Edit Cover</button>
                                <button class="cta cta-main cta-inline cta-cover-cancel btnCancelChangeCoverPic" style="display:none;">Cancel</button>
                            </div>
                            <div class="component component-v-spacing-sm component-style component-business-photo text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-12">
                                        <h5>Cover Pic</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="cover-photo-wrapper coverphoto edited-mode">
                                            <img class="cover-photo" id="coverpic" runat="server" src="/img/default-cover-photo.jpg"/>
                                        </div>
                                        <div class="image-editor coverphoto edit-mode" style="display:none;">
                                            <input type="file" class="cropit-image-input">
                                            <div class="cropit-preview"></div>
                                            <div class="image-size-label">
                                            Resize image
                                            </div>
                                            <input type="range" class="cropit-image-zoom-input">
                                            <button class="rotate-ccw">Rotate counterclockwise</button>
                                            <button class="rotate-cw">Rotate clockwise</button>

                                            <button class="export">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img id="xas" runat="server" src="blank" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>