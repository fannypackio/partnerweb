﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Teams.aspx.cs" Inherits="PartnerWeb.Members.Teams" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentJSPlaceHolder" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["webstatic"] %>/js/lib/jquery.tagsinput.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["webstatic"] %>/js/partner/teams.js"></script>
    <link rel="stylesheet" type="text/css" href="<%= ConfigurationManager.AppSettings["webstatic"] %>/css/lib/jquery.tagsinput.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" runat="server">
    <div class="section-admin-manage-business section-white-pure no-flex-add-padding">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentTeamLink" class="cta cta-underline admin-component-link cta-underline">Team</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentPhotosLink" href="/photos" class="cta admin-component-link">Photos</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentHoursLink" href="/hours" class="cta admin-component-link">Hours</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="admin-component active admin-component-team-members">
                        <div class="col-md-12">
                            <div class="component">
                                <h6 class="color-partner">Invite Code: <asp:Literal ID="litInviteCode" runat="server"></asp:Literal></h6>
                                <h6>Your team members can join on their own with your invite code, or you can send them invites via email below.</h6>
                                <button id="AddMembersButton" class="cta cta-main cta-add-members">+ Team Member</button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="component component-v-spacing-sm component-add-member" id="inviteBlock" runat="server">
                                    <h6>Enter the email addresses for the team members you'd like to invite - they will be sent an invite via email.</h6>
                                    <input id="invitelist" type="text" class="full-width text-field border-field" placeholder="ex. johndoe@example.com, janesmith@example.com, jakeross@example.com">
                                    <button id="SendInvitesButton" type="button" class="cta cta-main" name="button">Send Invites</button>
                                    <button type="button" class="cta cta-cancel-add" name="button">Cancel</button>
                            </div>
                            <div class="component component-v-spacing-sm component-add-member" id="billingBlock" runat="server">
                                <h6>Adding team members requires a payment method to be added</h6><a class="cta cta-main cta-green" href="/pricing">View Pricing</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <asp:Repeater ID="rpt_teammembers" runat="server" OnItemCreated="rpt_teammembers_ItemCreated">
                                <ItemTemplate>
                                    <div class="component component-v-spacing component-style team-member text-left">
                                        <div class="row row-flex row-flex-center">
                                            <div class="col-md-1">
                                                <div class="team-member-photo-wrapper">
                                                    <img id="empPhoto" runat="server" class="team-member-photo" src="<%= ConfigurationManager.AppSettings['webstatic'] %>/img/default-profile.png"/>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <h6 class="member-name"><asp:Label ID="lblName" runat="server"></asp:Label></h6>
                                            </div>
                                            <div class="col-md-4">
                                                <h6 class="member-email"><asp:Label ID="lblEmail" runat="server"></asp:Label></h6>
                                            </div>
                                            <div class="col-md-3">
                                                <a overlay-type="member-availability" class="cta cta-inline empavialability" id="empAvailabilityBtn" runat="server" >Make Unavailable</a>
                                            </div>
                                            <div class="col-md-2">
                                                <a overlay-type="member-remove" class="cta cta-inline cta-red empdelete" id="empRemoveBtn" runat="server">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hdn_PartnerID" runat="server" />
    <input type="hidden" id="hdn_PartnerInviteCode" runat="server" />
    <input type="hidden" id="hdn_PartnerName" runat="server" />
    <div style="display:none;">
        <div id="dialog-confirm-unavailable" title="Make Unavailable?">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>This team member will stop receiving calls. Are you sure?</p>
        </div>
        <div id="dialog-confirm-available" title="Make Available?">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>This team member will be able to take calls. Are you sure?</p>
        </div>
        <div id="dialog-confirm-delete" title="Delete Team Member?">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>This team member will be deleted. Are you sure?</p>
        </div>
    </div>
    
</asp:Content>
