﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="timings.aspx.cs" Inherits="PartnerWeb.Members.timings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentJSPlaceHolder" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["webstatic"] %>/js/partner/hours.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" runat="server">
    <div class="section-admin-manage-business section-white-pure no-flex-add-padding">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentTeamLink" href="/team" class="cta admin-component-link">Team</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentPhotosLink" href="/photos" class="cta admin-component-link">Photos</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentHoursLink" class="cta admin-component-link cta-underline">Hours</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="admin-component admin-component-hours active">
                        <div class="col-md-12">
                            <div class="component">
                                <button id="EditHours" class="cta cta-main cta-edit-hours">Edit Hours</button>
                                <button id="CancelHours" class="cta cta-main cta-cancel">Cancel</button>
                                <button id="SaveHours" class="cta cta-main cta-gap cta-save-hours">Save Hours</button>
                            </div>
                        </div>
                        <div class="col-md-12" ID="colhours" runat="server"> 
                            <asp:Repeater ID="rpt_hours" runat="server" OnItemCreated="rpt_hours_ItemCreated">
                                <ItemTemplate>
                                    <div class="component component-v-spacing-sm component-style component-business-hours text-left">
                                        <div class="row row-flex row-flex-center">
                                            <div class="col-md-2 col-sm-12 col-xs-12">
                                                <select id="daySelect" runat="server" class="enabler daySelect" disabled>
                                                  <option value="1">Monday</option>
                                                  <option value="2">Tuesday</option>
                                                  <option value="3">Wednesday</option>
                                                  <option value="4">Thursday</option>
                                                  <option value="5">Friday</option>
                                                  <option value="6">Saturday</option>
                                                  <option value="0">Sunday</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-4 text-center-desktop">
                                                <input class="text-field enabler starttime" type="time" disabled name="" id="starttime" runat="server" value="08:00">
                                            </div>
                                            <div class="col-md-1 col-sm-2 col-xs-2">
                                                <h6 class="enabler">to</h6>
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-4 text-center-desktop">
                                                <input class="text-field enabler endtime" type="time" disabled name="" id="endtime" runat="server" value="08:00">
                                            </div>
                                            <div class="col-md-3 col-sm-2 col-xs-2 text-center">
                                               <i class="fa fa-trash delete-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div id="extra-hidden-hours"style="display:none;">
                            <div class="component component-v-spacing-sm component-style component-business-hours text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-2 col-sm-12 col-xs-12">
                                        <select runat="server" class="enabler daySelect">
                                            <option value="1">Monday</option>
                                            <option value="2">Tuesday</option>
                                            <option value="3">Wednesday</option>
                                            <option value="4">Thursday</option>
                                            <option value="5">Friday</option>
                                            <option value="6">Saturday</option>
                                            <option value="0">Sunday</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-4 text-center-desktop">
                                        <input class="text-field enabler starttime" type="time" disabled name="" value="08:00">
                                    </div>
                                    <div class="col-md-1 col-sm-2 col-xs-2">
                                        <h6 class="enabler">to</h6>
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-4 text-center-desktop">
                                        <input class="text-field enabler endtime" type="time" disabled name="" value="08:00">
                                    </div>
                                    <div class="col-md-3 col-sm-2 col-xs-2 text-center">
                                        <i class="fa fa-trash delete-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button id="AddHours" class="cta cta-main cta-add-hours">+ Add Row</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hdn_PartnerID" runat="server" />
</asp:Content>
