﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Drawing;
using System.Configuration;

namespace PartnerWeb.Members
{
    public partial class photos : System.Web.UI.Page
    {
        Partner oPartner = null;
        public string coverPhotoURL = "";
        public string profilePhotoURL = "";
        public string gpartnerid = "";
        protected override void OnPreRender(EventArgs e)
        {
            ((Site)Master).setPageTitle("Jumper Photos");
            ((Site)Master).setActiveHeaderLink("manage");
            ((Site)Master).setPageDesc("Manage your business photos on Jumper");
            base.OnPreRender(e);

        }
        public void Page_Load(object source, EventArgs e)
        {

            if (!IsPostBack)
            {
                HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
                if (userCookie == null)
                {
                    Response.Redirect("login");
                }
                else
                {
                    //user logged in
                    //get value from cookie
                    //get user details
                    //get partner details
                    string access_token = userCookie.Value;
                    getUserDetails(access_token);
                }
            }
        }
        private void getUserDetails(string token)
        {
            PartnerWeb.User _user = ((Site)Master).getUserDetails();
            gpartnerid = _user.PartnerID.ToString();
            getPartnerPhotos(_user.PartnerID);
        }
        private void getPartnerPhotos(int partnerid)
        {
            //partnerid = 112;
            if (partnerid > 0)
            {
                string url = ConfigurationManager.AppSettings["JumperCustomer.API.Url"] + "/partners/ID/" + partnerid;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                //request.Headers.Add("Authorization", "bearer " + token);
                request.ContentType = "application/json";
                HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
                string responseText;

                using (var webresponse = request.GetResponse())
                {
                    using (var reader = new StreamReader(webresponse.GetResponseStream()))
                    {
                        responseText = reader.ReadToEnd();

                       oPartner = JsonConvert.DeserializeObject<Partner>(responseText);
                        if (oPartner!=null)
                        {
                            //oPartner = listPartner[0];
                            loadPhotos();
                        }
                        else
                        {
                            //blank look
                        }
                    }
                }
                //return _CustomerInfo;
            }
            else
            {
                //User not Connected to business
            }
        }
        private void loadPhotos()
        {
            xas.Src = "";
            coverpic.Src = oPartner.CoverPhotoURL;
            profilepic.Src = oPartner.ProfilePhotoURL;
            
            //https://d15bqe4cdm89bh.cloudfront.net/fit-in/a4e0fc6f-6fb3-4a93-b935-ccd088e69d82
            coverPhotoURL = oPartner.CoverPhotoURL.Replace("https://d15bqe4cdm89bh.cloudfront.net/fit-in", "http://s3.us-west-2.amazonaws.com/testjumper");
            //https://d15bqe4cdm89bh.cloudfront.net/fit-in/33c68a19-a070-4e4c-a4de-52efd1b942c1
            profilePhotoURL = oPartner.ProfilePhotoURL.Replace("https://d15bqe4cdm89bh.cloudfront.net/fit-in", "http://s3.us-west-2.amazonaws.com/testjumper");
        }
    }
}