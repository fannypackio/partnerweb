﻿
<%@ Page Language="C#" Inherits="PartnerWeb.plans" MasterPageFile="~/Site.master" CodeBehind="~/plans.aspx.cs" %>


<asp:Content ID="PageContent" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="section-admin-manage-business section-white-pure no-flex-add-padding">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="component component-v-spacing text-center">
                        <a href="/pricing" id="ComponentPlansLink" class="cta cta-underline admin-component-link">Pricing</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="component component-v-spacing text-center">
                        <a href="/billing" id="ComponentBillingLink" class="cta admin-component-link">Billing</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="admin-component active admin-component-plans">
                        <div class="col-md-12">
                            <div class="component">
<!--                                <h6>Current Plan: <span class="plan-active"><asp:Literal ID="litPlanName" runat="server"></asp:Literal></span></h6>-->
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="PlanIndividual" Runat="Server" class="component component-v-spacing component-plan">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Individual</h3>
                                        <h5>For single business owners and managers</h5>
                                        <h6>Includes 1 admin + unlimited video calling</h6>
                                    </div>
                                    <div class="col-md-3 text-center col-sm-12 col-xs-12">
                                        <h3 class="price">Free</h3>
                                        <h6>For individuals</h6>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12" style="visibility:hidden;">
                                        <button overlay-type="plan" class="cta cta-main cta-select-plan overlay-trigger" data-planid="<%= ConfigurationManager.AppSettings["Stripe.Plan.Free"] %>">Select</button>
                                    </div>
                                </div>
                            </div>
                            <div id="PlanSmall" Runat="Server" class="component component-v-spacing component-plan">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Additional Team Members</h3>
                                        <h5>Invite your team to join your Partner account</h5>
                                        <h6>Includes 1 admin & unlimited team members + unlimited video calling</h6>
                                    </div>
                                    <div class="col-md-3 text-center col-sm-12 col-xs-12">
                                        <h3 class="price">$19/month</h3>
                                        <h6>Per additional team member</h6>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <a class="cta cta-main cta-green cta-select-plan" id="btn_billing" runat="server" visible="false" href="/billing">Add Payment Method</a>
                                        <%--<button overlay-type="plan" class="cta cta-main cta-select-plan overlay-trigger" data-planid="<%= ConfigurationManager.AppSettings["Stripe.Plan.Paid"] %>">Select</button>--%>
                                    </div>
                                </div>
                            </div>
                            <div id="PlanEnterprise" runat="server" class="component component-v-spacing component-plan component-plan-enterprise">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Enterprise</h3>
                                        <h5>Got a large team?</h5>
                                        <h6>Includes custom features and discounts</h6>
                                    </div>
                                    <div class="col-md-3 text-center col-sm-12 col-xs-12">
                                        <h3>Contact Us</h3>
                                        <h6>For more information</h6>
                                    </div>
                                    <div class="col-md-3 text-center col-sm-12 col-xs-12">
                                        <a href="tel:+19494245329" class="cta cta-main cta-select-plan">(949) 424-5329</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">


    </script>
</asp:Content>