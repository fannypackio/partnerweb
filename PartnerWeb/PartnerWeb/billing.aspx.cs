﻿using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;

namespace PartnerWeb
{

    public partial class plans_and_billing : System.Web.UI.Page
    {
        private PartnerWeb.User _user = null;
        private StripeCard _CurrentCard = null;
        PartnerBilling _PartnerBilling = null;
        protected override void OnPreRender(EventArgs e)
        {
            ((Site)Master).setPageTitle("Jumper Billing");
            ((Site)Master).setPageDesc("Manage your billing on Jumper");
            ((Site)Master).setActiveHeaderLink("plans");
            base.OnPreRender(e);

        }
        public void Page_Load(object source, EventArgs e)
        {
            String access_token = "";
            HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
            if (userCookie == null)
            {
                Response.Redirect("login");
            }
            else
            {
                access_token = userCookie.Value;
                getUserDetails(access_token);

            }
            
            if (IsPostBack && _user != null)
            {
                if (!string.IsNullOrEmpty(hdn_stripetoken.Value))
                {
                    //Credit Card changed and Stripe Validated caard info
                    //Make Api call to save and create billing in Stripe for that customer

                    //Read Card Details
                    //Get Token
                    //Api call to save card details
                    /*string cardNumber = string.Empty;
                    string expiry = string.Empty;
                    string cvcNumber = string.Empty;
                    string zipcode = string.Empty;*/
                    string TokenId = hdn_stripetoken.Value;
                    string PartnerId = _user.PartnerID.ToString();
                    postPaymentDetails(PartnerId,TokenId, access_token);
                    ((Site)Master).showContinueError("You’re set up! You can now invite your team members.", "Click here", "/team");
                    hdn_stripetoken.Value = "";
                }
                else
                {
                    string usercoupon = txt_coupon.Value.ToUpper();
                    if (!string.IsNullOrEmpty(usercoupon))
                    {
                        //save coupon to customer in Stripe
                        if (_PartnerBilling != null)
                        {
                            try
                            {   
                                StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["Stripe.API.SecretKey"]);
                                var customerService = new StripeCustomerService();
                                var options = new StripeCustomerUpdateOptions
                                {
                                    Coupon = usercoupon
                                };
                                StripeCustomer stripecustomer = customerService.Update(_PartnerBilling.PaymentCustomerID, options);//"cus_DMadQNAyKCGdnq"
                                litAddedCoupon.Text = usercoupon;
                            }
                            catch(Exception ex)
                            {
                                ((Site)Master).showError("Invalid coupon code");
                            }
                        }
                    }

                }
            }
            if (_CurrentCard != null && !string.IsNullOrEmpty(_CurrentCard.Id))
            {
                litCurrentCard.Text = _CurrentCard.Brand + " card ending with " + _CurrentCard.Last4;
                msg_coupondisabled.Visible = false;
                ChangePromoButton.Visible = true;
            }
            else
            {
                litCurrentCard.Text = "None";
                msg_coupondisabled.Visible = true;
                ChangePromoButton.Visible = false;
            }
        }

        private void getUserDetails(string token)
        {
            _user = ((Site)Master).getUserDetails();

            getPartnerBilling(_user.PartnerID,token);
            if (_PartnerBilling != null)
            {
                StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["Stripe.API.SecretKey"]);

                var customerService = new StripeCustomerService();
                StripeCustomer stripecustomer = customerService.Get(_PartnerBilling.PaymentCustomerID);//"cus_DMadQNAyKCGdnq"
                //Credit Card
                if (stripecustomer.Sources!=null && stripecustomer.Sources.TotalCount > 0)
                {
                    _CurrentCard = stripecustomer.Sources.Data[0].Card;
                    
                }
                //Discount
                if(stripecustomer.StripeDiscount!= null)
                {
                    litAddedCoupon.Text = stripecustomer.StripeDiscount.StripeCoupon.Id;
                }
                StripeSubscription _FreeSubscription = null;
                StripeSubscription _PaidSubscription = null;
                int TotalTeamCount = 0;
                double TotalPrice = 0.0;
                double DiscountedTotalPrice = 0.0;
                //Products,Plans,Subscriptions
                if (stripecustomer.Subscriptions != null && stripecustomer.Subscriptions.TotalCount > 0)
                {
                    foreach(StripeSubscription ss in stripecustomer.Subscriptions)
                    {
                        if(ss.StripePlan.Amount >0 && _PaidSubscription == null)
                        {
                            _PaidSubscription = ss;
                        }else if (ss.StripePlan.Amount == 0 && _FreeSubscription == null)
                        {
                            _FreeSubscription = ss;
                        }
                        //StripeList
                        TotalTeamCount += (int)ss.Quantity;

                        if (stripecustomer.StripeDiscount != null && stripecustomer.StripeDiscount.StripeCoupon.LiveMode && stripecustomer.StripeDiscount.StripeCoupon.PercentOff > 0)
                        {
                            double discountedPrice = (float)ss.StripePlan.Amount * (float)((100 - (float)stripecustomer.StripeDiscount.StripeCoupon.PercentOff) / 100);
                            DiscountedTotalPrice += (float)ss.Quantity * discountedPrice;
                            spnDiscountedTotalAmount.Visible = true;
                            spnTotalAmount.Attributes["class"] += " price-old";
                        }
                        TotalPrice += (float)ss.Quantity * (float)ss.StripePlan.Amount;
                    }

                    if (_PaidSubscription != null)
                    {
                        //litPlanName.Text = _PaidSubscription.StripePlan.Nickname;
                        float fPlanAmt = (float)_PaidSubscription.StripePlan.Amount / 100;
                        litPlanPrice.Text = String.Format("{0:C}", fPlanAmt);
                        if (stripecustomer.StripeDiscount != null && stripecustomer.StripeDiscount.StripeCoupon.LiveMode && stripecustomer.StripeDiscount.StripeCoupon.PercentOff > 0)
                        {
                            double discountedPlanPrice = (float)fPlanAmt * (float)((100 - (float)stripecustomer.StripeDiscount.StripeCoupon.PercentOff) / 100);
                            litDiscountedPlanPrice.Text = String.Format("{0:C}", discountedPlanPrice);
                            spnDiscountedPlanPrice.Visible = true;
                            spnPlanPrice.Attributes["class"] += " price-old";
                        }
                    }
                    else
                    {
                        //litPlanName.Text = _FreeSubscription.StripePlan.Nickname;
                        float fPlanAmt = (float)_FreeSubscription.StripePlan.Amount / 100;
                        litPlanPrice.Text = String.Format("{0:C}", fPlanAmt);
                    }
                    litTeamCount.Text = TotalTeamCount.ToString();
                    //TotalPrice = 1999;
                    float fTotal = (float)TotalPrice / 100;
                    litTotalAmount.Text = String.Format("{0:C}", fTotal);// fTotal.ToString();
                    litDiscountedTotalAmount.Text = String.Format("{0:C}", ((float)DiscountedTotalPrice / 100));// fTotal.ToString();
                }

            }
            //getPartnerBillingCards(_user.PartnerID, token);
            //_user.PartnerID = 112;
            //hdn_PartnerID.Value = _user.PartnerID.ToString();
        }
        /*
        private void getPartnerBillingCards(int partnerid, string token)
        {
            StripeCard _PartnerBillingCard = null;
            string url = "http://partnerapi.techscotch.com/api/Account/GetPartnerBillingCardDetails";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add("Authorization", "bearer " + token);
            request.ContentType = "application/json";
            HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
            string responseText;

            using (var webresponse = request.GetResponse())
            {
                using (var reader = new StreamReader(webresponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    List<StripeCard> ListPartnerBillingCards = JsonConvert.DeserializeObject<List<StripeCard>>(responseText);
                    if (ListPartnerBillingCards.Count > 0)
                    {
                        _PartnerBillingCard = ListPartnerBillingCards[0];
                        //inviteBlock.Visible = true;
                        //billingBlock.Visible = false;
                        litCurrentCard.Text = _PartnerBillingCard.Brand + " card ending with " + _PartnerBillingCard.Last4;
                    }
                    else
                    {
                        litCurrentCard.Text = "None";
                    }
                }
            }
            //return _CustomerInfo;
        }
        */
        /*
        private Boolean postPaymentDetails(string partnerID, string token, string access_token)
        {
            bool result = false;
            BillingCard bc = new BillingCard();
            bc.Token = token;
            bc.PartnerID = partnerID;
            //OperatingHours oh = JsonConvert.DeserializeObject<OperatingHours>(content);
            using (var wb = new WebClient())
            {
                wb.Headers["content-type"] = "application/json";
                wb.Headers["Authorization"] = "bearer " + access_token;
                //string url = "http://localhost:52380/api/Account/AddBilling";
                string url = "http://partnerapi.techscotch.com/api/Account/AddBilling";
                var response = wb.UploadData(url, "POST", Encoding.Default.GetBytes(JsonConvert.SerializeObject(bc)));
                string responseInString = Encoding.UTF8.GetString(response);
            }
            return result;
        }
        */

        private void postPaymentDetails(string partnerID, string token, string access_token)
        {
            var cardService = new StripeCardService();
            
            if (_CurrentCard != null && !string.IsNullOrEmpty(_CurrentCard.Id))
            {
                //Delete Current Card
                StripeDeleted deletedCard = cardService.Delete(_PartnerBilling.PaymentCustomerID, _CurrentCard.Id);
            }
            //Add New Card
            var cardOptions = new StripeCardCreateOptions()
            {
                SourceToken = token
            };
            _CurrentCard = cardService.Create(_PartnerBilling.PaymentCustomerID, cardOptions);

        }
        private void getPartnerBilling(int partnerid, string token)
        {
           
            string url = ConfigurationManager.AppSettings["JumperPartner.API.Url"] + "/Account/GetPartnerBilling?partnerID=" + partnerid;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add("Authorization", "bearer " + token);
            request.ContentType = "application/json";
            HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
            string responseText;

            using (var webresponse = request.GetResponse())
            {
                using (var reader = new StreamReader(webresponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                    List<PartnerBilling> ListPartnerBilling = JsonConvert.DeserializeObject<List<PartnerBilling>>(responseText);
                    if (ListPartnerBilling.Count > 0)
                    {
                        _PartnerBilling = ListPartnerBilling[0];
                        //inviteBlock.Visible = true;
                        //billingBlock.Visible = false;
                    }
                }
            }
            //return _CustomerInfo;
        }
    }
}
public class BillingCard
{
    public string Token { get; set; }
    public string PartnerID { get; set; }
}
