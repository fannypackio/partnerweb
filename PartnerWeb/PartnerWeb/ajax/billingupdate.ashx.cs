﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PartnerWeb.ajax
{
    /// <summary>
    /// Summary description for billingupdate
    /// </summary>
    public class billingupdate : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Stream req = context.Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);

            string json = new StreamReader(req).ReadToEnd();
            StripeEvent stripeEvent = null;
            Boolean isValidRequest = false;
            try
            {
                stripeEvent = StripeEventUtility.ParseEvent(json);
                isValidRequest = true;
            }
            catch (Exception ex)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Unable to parse incoming event");
            }
            
            if (isValidRequest)
            {
                if (stripeEvent != null)
                {
                    switch (stripeEvent.Type)
                    {
                        case "charge.refunded":
                            // do work
                            break;
                        case "customer.subscription.updated":
                        case "customer.subscription.deleted":
                        case "customer.subscription.created":
                            // do work
                            break;
                        case "charge.succeeded":
                            // Valid Payment - Update Partner Record to be valid
                            //string stripeCustomerID = stripeEvent.Data.Object["Source"];
                            /*
                            StripeCharge subscription = Stripe.Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());
                            if (subscription != null)
                            {
                                string customerid = subscription.CustomerId.ToString();
                                var customerService = new StripeCustomerService();
                                StripeCustomer stripecustomer = customerService.Get(customerid);
                                if (stripecustomer != null && !string.IsNullOrEmpty(stripecustomer.Email))
                                {
                                    KeepPartnerActive(stripecustomer.Email);
                                }
                            }
                            */
                            break;
                        case "customer.source.expiring":
                            // Card is expiring soon - send notification - later
                            break;
                        case "invoice.payment_failed":
                            // Card is expiring soon - send notification - later
                            StripeInvoice StripeInvoiceFailed = Stripe.Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());
                            if (StripeInvoiceFailed != null)
                            {
                                string customerid = StripeInvoiceFailed.CustomerId.ToString();
                                var customerService = new StripeCustomerService();
                                StripeCustomer stripecustomer = customerService.Get(customerid);
                                if (stripecustomer != null && !string.IsNullOrEmpty(stripecustomer.Email))
                                {
                                    DeactivatePartner(stripecustomer.Email);
                                }
                            }
                            break;
                        case "invoice.payment_succeeded":
                            StripeInvoice StripeInvoiceSuccess = Stripe.Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());
                            if (StripeInvoiceSuccess != null)
                            {
                                string customerid = StripeInvoiceSuccess.CustomerId.ToString();
                                var customerService = new StripeCustomerService();
                                StripeCustomer stripecustomer = customerService.Get(customerid);
                                if (stripecustomer != null && !string.IsNullOrEmpty(stripecustomer.Email))
                                {
                                    KeepPartnerActive(stripecustomer.Email);
                                }
                            }
                            break;
                        case "source.failed":
                            // Payment Failed - deactivate Sales rep
                            /*
                            StripeCharge subscriptionFailed = Stripe.Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());
                            if (subscriptionFailed != null)
                            {
                                string customerid = subscriptionFailed.CustomerId.ToString();
                                var customerService = new StripeCustomerService();
                                StripeCustomer stripecustomer = customerService.Get(customerid);
                                if (stripecustomer != null && !string.IsNullOrEmpty(stripecustomer.Email))
                                {
                                    DeactivatePartner(stripecustomer.Email);
                                }
                            }
                            */
                            break;

                    }
                }
            }

            //context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }

        private void KeepPartnerActive(string email)
        {
            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();
                data["Email"] = email;
                data["Status"] = "1";
                string url = ConfigurationManager.AppSettings["JumperCustomer.API.Url"] + "/Partners/UpdatePaymentStatus";
                var response = wb.UploadValues(url, "POST", data);
                string responseInString = Encoding.UTF8.GetString(response);
            }
        }
        private void DeactivatePartner(string email)
        {
            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();
                data["Email"] = email;
                data["Status"] = "0";
                string url = ConfigurationManager.AppSettings["JumperCustomer.API.Url"] + "/Partners/UpdatePaymentStatus";
                var response = wb.UploadValues(url, "POST", data);
                string responseInString = Encoding.UTF8.GetString(response);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}