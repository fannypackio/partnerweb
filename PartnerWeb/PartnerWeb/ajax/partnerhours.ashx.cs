﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PartnerWeb.ajax
{
    /// <summary>
    /// Summary description for partnerhours
    /// </summary>
    public class partnerhours : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string content = "";
            context.Response.ContentType = "text/plain";
            using (var reader = new StreamReader(context.Request.InputStream))
            {
                content = reader.ReadToEnd();
                List<OperatingHours> oh = JsonConvert.DeserializeObject<List<OperatingHours>>(content);
                using (var wb = new WebClient())
                {
                    wb.Headers["content-type"] = "application/json";
                    string url = ConfigurationManager.AppSettings["JumperCustomer.API.Url"] + "/Partners/UpdateHours";
                    var response = wb.UploadData(url, "POST", Encoding.Default.GetBytes(JsonConvert.SerializeObject(oh)));
                    string responseInString = Encoding.UTF8.GetString(response);
                }
            }

            context.Response.Write(context);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}