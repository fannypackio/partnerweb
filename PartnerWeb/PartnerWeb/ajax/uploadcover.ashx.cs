﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace PartnerWeb.ajax
{
    /// <summary>
    /// Summary description for uploadcover
    /// </summary>
    public class uploadcover : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            //get PartnerID
            //Read Input file stream

            //Upload Photo - send stream
            //Save it to Partner Cover
            var httpRequest = HttpContext.Current.Request;
            int partnerID = int.Parse(httpRequest["partnerid"]);
            try
            {
                if(httpRequest["file"]!=null && !string.IsNullOrEmpty(httpRequest["file"]))
                {
                    string[] fileData = httpRequest["file"].Split(',');

                    string bucketName = "testjumper";
                    string keyName = Guid.NewGuid().ToString();
                    var client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1);

                    try
                    {
                        byte[] bytes = Convert.FromBase64String(fileData[1]);
                        using (client)
                        {
                            var request = new PutObjectRequest
                            {
                                BucketName = bucketName,
                                CannedACL = S3CannedACL.PublicRead,
                                ContentType = "image/jpg",
                                Key = keyName
                            };
                            using (var ms = new MemoryStream(bytes))
                            {
                                request.InputStream = ms;
                                client.PutObject(request);
                                Partner oPartner = new Partner();
                                oPartner.ID = partnerID;
                                oPartner.CoverPhoto = keyName;
                                using (var wb = new WebClient())
                                {
                                    wb.Headers["content-type"] = "application/json";
                                    string url = ConfigurationManager.AppSettings["JumperCustomer.API.Url"] + "/Partners/UpdatePhoto";
                                    var response = wb.UploadData(url, "POST", Encoding.Default.GetBytes(JsonConvert.SerializeObject(oPartner)));
                                    string responseInString = Encoding.UTF8.GetString(response);
                                }
                            }
                            context.Response.StatusCode = (int)HttpStatusCode.OK;
                            context.Response.Write(
                                JsonConvert.SerializeObject(
                                    new
                                    {
                                        photoid = keyName,
                                        url = "https://d15bqe4cdm89bh.cloudfront.net/fit-in/"+keyName
                                    }
                                )
                            );
                        }
                    }
                    catch (AmazonS3Exception amazonS3Exception)
                    {
                        string message = "";
                        if (amazonS3Exception.ErrorCode != null &&
                            (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                            ||
                            amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                        {
                            message = "Check the provided AWS Credentials.";
                        }
                        else
                        {
                            message = "Error occurred: " + amazonS3Exception.Message;
                        }
                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        context.Response.Write(
                            JsonConvert.SerializeObject(
                                new
                                {
                                    error = "something is wrong"
                                }
                            )
                        );
                    }
                    
                }
            }
            catch(Exception ex)
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}