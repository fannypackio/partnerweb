﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using System.Configuration;

namespace PartnerWeb.ajax
{
    /// <summary>
    /// Summary description for empstatus
    /// </summary>
    public class empstatus : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            //context.Response.Write("Hello World");
            string empID = context.Request["id"];
            string available = context.Request["available"];
            HttpCookie cookie = (HttpCookie)HttpContext.Current.Request.Cookies["PartnerCookie"];
            string token = cookie.Value;
            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();
                data["Id"] = empID;
                data["IsAvailable"] = available;
                string url = ConfigurationManager.AppSettings["JumperPartner.API.Url"] + "/account/EmployeeAvailability";
                wb.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
                var response = wb.UploadValues(url, "POST", data);
                string responseInString = Encoding.UTF8.GetString(response);
            }
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            context.Response.Write(
                JsonConvert.SerializeObject(
                    new
                    {
                        available = available
                    }
                )
            );
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}