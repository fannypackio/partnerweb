﻿using System;
using System.Web;
using System.Web.UI;

namespace PartnerWeb
{

    public partial class signout : System.Web.UI.Page
    {
		public void Page_Load(object source, EventArgs e)
        {
            if (!IsPostBack)
            {
				Response.Cookies.Remove("PartnerCookie");
				Response.Redirect("login.aspx?ref=so");
            }
        }
    }
}
