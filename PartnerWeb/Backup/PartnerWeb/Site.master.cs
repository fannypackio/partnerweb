﻿using System;
using System.Web;
using System.Web.UI;
namespace PartnerWeb
{
    public partial class Site : System.Web.UI.MasterPage
    {   
		public void setPageTitle(String Title)
        {
			pageTitle.Text = Title;
        }
		public void hideHeaderLinks(){
			pillnav.Visible = false;
			headerlinks.Visible = false;
		}
		public void showError(String message){

			errorWrap.Attributes["class"] += " active";
			errorLabel.InnerText = message;
		}
        public void setPageDesc(String Desc) {
            pageDesc.Attributes["content"] = Desc;
            pageDescAlt.Attributes["content"] = Desc;
            pageDescFb.Attributes["content"] = Desc;
            pageDescTwt.Attributes["content"] = Desc;
        }
        public void setActiveHeaderLink(String pageName) {
            if (pageName == "manage") {
                managebusinesslink.Attributes["class"] += (" " + "cta-underline");
            } else if (pageName == "plans") {
                planslink.Attributes["class"] += (" " + "cta-underline");
            } else {
                
            }
        }
    }
}
