﻿<%@ Page Language="C#" Inherits="PartnerWeb.team" MasterPageFile="~/Site.master"%>
<asp:Content id="PageJSContent" ContentPlaceHolderID="contentJSPlaceHolder" runat="server">
    <script src="//127.0.0.1:8082/js/partner/team.js"></script>
</asp:Content>
<asp:Content ID="PageContent" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="section-admin-manage-business section-white-pure">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentTeamLink" class="cta cta-underline admin-component-link cta-underline">Team</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentPhotosLink" href="photo.aspx" class="cta admin-component-link">Photos</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentHoursLink" href="Members/Hours.aspx" class="cta admin-component-link">Hours</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="admin-component active admin-component-team-members">
                        <div class="col-md-12">
                            <div class="component">
                                <h6 class="color-partner">Invite Code: 123456</h6>
                                <h6>Your team members can join on their own with your invite code, or you can send them invites via email below.</h6>
                                <button id="AddMembersButton" class="cta cta-main cta-add-members">+ Team Member</button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="component component-v-spacing-sm component-add-member">
                                    <h6>Enter the email addresses for the team members you'd like to invite - they will be sent an invite via email.</h6>
                                    <input type="text" class="full-width text-field border-field" placeholder="ex. johndoe@example.com, janesmith@example.com, jakeross@example.com">
                                    <button id="SendInvitesButton" type="button" class="cta cta-main" name="button">Send Invites</button>
                                    <button type="button" class="cta cta-cancel-add" name="button">Cancel</button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="component component-v-spacing component-style team-member text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-1">
                                        <div class="team-member-photo-wrapper">
                                            <img class="team-member-photo" src="//127.0.0.1:8082/img/default-profile.png"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 class="member-name">John Doe</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <h6>johndoe@carmax.com</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <a overlay-type="member-availability" class="cta cta-inline cta-red overlay-trigger">Make Unavailable</a>
                                        <a overlay-type="member-remove" class="cta cta-inline cta-red overlay-trigger">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="component component-v-spacing component-style team-member text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-1">
                                        <div class="team-member-photo-wrapper">
                                            <img class="team-member-photo" src="//127.0.0.1:8082/img/default-profile.png"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 class="member-name">John Doe</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <h6>johndoe@carmax.com</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <a overlay-type="member-availability" class="cta cta-inline cta-red overlay-trigger">Make Unavailable</a>
                                        <a overlay-type="member-remove" class="cta cta-inline cta-red overlay-trigger">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="component component-v-spacing component-style team-member text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-1">
                                        <div class="team-member-photo-wrapper">
                                            <img class="team-member-photo" src="//127.0.0.1:8082/img/default-profile.png"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 class="member-name">John Doe</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <h6>johndoe@carmax.com</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <a overlay-type="member-availability" class="cta cta-inline cta-red overlay-trigger">Make Unavailable</a>
                                        <a overlay-type="member-remove" class="cta cta-inline cta-red overlay-trigger">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="component component-v-spacing component-style team-member text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-1">
                                        <div class="team-member-photo-wrapper">
                                            <img class="team-member-photo" src="//127.0.0.1:8082/img/default-profile.png"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 class="member-name">John Doe</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <h6>johndoe@carmax.com</h6>
                                    </div>
                                    <div class="col-md-4">
                                        <a overlay-type="member-availability" class="cta cta-inline cta-red overlay-trigger">Make Unavailable</a>
                                        <a overlay-type="member-remove" class="cta cta-inline cta-red overlay-trigger">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>