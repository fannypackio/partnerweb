﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace PartnerWeb
{

    public partial class Hours : System.Web.UI.Page
    {
		public void Page_Load(object source, EventArgs e)
        {
			
            if (!IsPostBack)
            {
                HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
                if (userCookie == null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //user logged in
                    //get value from cookie
                    //get user details
                    //get partner details
                    string access_token = userCookie.Value;
                    getUserDetails(access_token);
                    
                }
                
            }
		}
		private void getUserDetails(string token)
        {
            PartnerWeb.User _user = PartnerWeb.User.GetUserInfo(token);
            getPartnerDetails(_user.PartnerID);
        }
		private void getPartnerDetails(int partnerid)
        {
			partnerid = 112;
            if (partnerid > 0)
            {
				string url = "http://customerapi.techscotch.com/api/OperatingHours/partner/"+partnerid;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                //request.Headers.Add("Authorization", "bearer " + token);
                request.ContentType = "application/json";
                HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
                string responseText;

                using (var webresponse = request.GetResponse())
                {
                    using (var reader = new StreamReader(webresponse.GetResponseStream()))
                    {
                        responseText = reader.ReadToEnd();
						List<OperatingHours> listHours = JsonConvert.DeserializeObject<List<OperatingHours>>(responseText);
						if (listHours.Count > 0)
						{/*
							Repeater rpt_hours = (Repeater)Page.FindControl("rpt_hours");
							rpt_hours.DataSource = listHours;
							rpt_hours.DataBind();
							*/
							
						}
						else{
							//blank look
						}
                    }
                }
                //return _CustomerInfo;
            }
            else
            {
                //User not Connected to business
            }
        }

		public void rpt_hours_ItemCreated(Object Sender, RepeaterItemEventArgs e){
			
		}
    }
}
