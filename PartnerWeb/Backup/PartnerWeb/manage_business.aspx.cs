﻿using System;
using System.Web;
using System.Web.UI;

namespace PartnerWeb
{

    public partial class manage_business : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            ((Site)Master).setPageTitle("Manage Business");
            ((Site)Master).setPageDesc("Manage your business and team on Jumper");
            ((Site)Master).setActiveHeaderLink("manage");
            base.OnPreRender(e);
        }
        public void Page_Load(object source, EventArgs e)
        {
			if (!IsPostBack)
            {
                HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
                if (userCookie == null)
                {
                    Response.Redirect("login.aspx");
				}else{
					//user logged in
					//get value from cookie
					//get user details
					    //get partner details
					string access_token = userCookie.Value;
					getUserDetails(access_token);

				}
            }
        }
		private void getUserDetails(string token){
			PartnerWeb.User _user = PartnerWeb.User.GetUserInfo(token);
			getPartnerDetails(_user.PartnerID);
		}
        private void getPartnerDetails(int partnerid)
        {
			if(partnerid >0){
				//
			}else{
				//User not Connected to business
			}
        }
    }
}
