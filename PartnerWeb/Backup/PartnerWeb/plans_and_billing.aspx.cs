﻿using System;
using System.Web;
using System.Web.UI;

namespace PartnerWeb
{

    public partial class plans_and_billing : System.Web.UI.Page
    {
        protected override void OnPreRender(EventArgs e)
        {
            ((Site)Master).setPageTitle("Plans & Billing");
            ((Site)Master).setPageDesc("Manage your plan and billing on Jumper");
            ((Site)Master).setActiveHeaderLink("plans");
            base.OnPreRender(e);

        }
        public void Page_Load(object source, EventArgs e)
        {
        }
    }
}
