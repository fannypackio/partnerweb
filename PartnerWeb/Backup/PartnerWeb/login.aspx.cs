﻿using System;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Specialized;

namespace PartnerWeb
{

    public partial class login : System.Web.UI.Page
    {
		protected override void OnPreRender(EventArgs e)
        {
			((Site)Master).hideHeaderLinks();
			((Site)Master).setPageTitle("Jumper Partner Login");
            ((Site)Master).setPageDesc("Log in to your Jumper Partner account");
            base.OnPreRender(e);
        }

		public void Page_Load(object source, EventArgs e)
		{
			if (!IsPostBack)
			{
               /* HttpCookie currentUserCookie = HttpContext.Current.Request.Cookies["PartnerCookie"];
                Response.Cookies.Remove("PartnerCookie");
				currentUserCookie.Expires = DateTime.Now.AddDays(-1);
                currentUserCookie.Value = null;
                Response.SetCookie(currentUserCookie);
                */

				if(Request.QueryString["ref"]!=null && Request.QueryString["ref"].ToString() == "so"){
					Response.Cookies.Remove("PartnerCookie");
				}else{
                    HttpCookie userCookie = Request.Cookies.Get("PartnerCookie");
                    if (userCookie != null && !string.IsNullOrEmpty(userCookie.Value))
                    {
                        Response.Redirect("team.aspx");
                    }
				}
			}
		}
		protected void Btn_Login_Click(object sender, EventArgs e)
        {
			string email = txt_email.Value;
			string password = txt_password.Value;

			if(!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password)){
               

				var request = (HttpWebRequest)WebRequest.Create("http://partnerapi.techscotch.com/token");

				string postData = "username=" + email + "&password=" + password + "&grant_type=password";
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
				try
				{
					var response = (HttpWebResponse)request.GetResponse();

					var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    LoginToken token = JsonConvert.DeserializeObject<LoginToken>(responseString);


                    if (!string.IsNullOrEmpty(token.access_token))
                    {
                        string a_token = token.access_token;
						HttpCookie userCookie = new HttpCookie("PartnerCookie");
                        DateTime now = DateTime.Now;
						userCookie.Value = a_token.ToString();
						userCookie.Expires = now.AddMonths(1);
						Response.SetCookie(userCookie);
                        Response.Redirect("manage_business.aspx");
                    }
                    else
                    {
                        ((Site)Master).showError("Invalid Username/Password");
                    }
				}
				catch(WebException wex){
					((Site)Master).showError("Invalid Username/Password");
				}            
			}
			else{
				//Empty fields
                ((Site)Master).showError("All fields are mandatory");
			}         
        }
		private async Task<Boolean> SignIn(string Email, string Password)
        {
			HttpCookie myCookieread = Request.Cookies["PartnerCookie"];

            // Read the cookie information and display it.
			if (myCookieread != null){
				string val = myCookieread.Value;
			}
			
            var client = new HttpClient();
			client.BaseAddress = new Uri("http://partnerapi.techscotch.com/api");

            string data = "username=" + Email + "&password=" + Password + "&grant_type=password";

            var content = new StringContent(data, Encoding.UTF8, "application/text");
            HttpResponseMessage response = await client.PostAsync("/token", content);

            // this result string should be something like: "{"token":"rgh2ghgdsfds"}"
            var result = await response.Content.ReadAsStringAsync();
            
            LoginToken token = JsonConvert.DeserializeObject<LoginToken>(result);

           
            if (!string.IsNullOrEmpty(token.access_token))
            {
				string x = token.access_token;

                return true;
            }
            else
            {
				((Site)Master).showError("Invalid Username/Password");
                return false;
            }
            //return new HttpWebResponse("", "");
        } 
	}
}
