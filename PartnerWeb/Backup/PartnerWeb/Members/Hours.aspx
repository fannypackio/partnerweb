﻿<%@ Page Language="C#" Inherits="PartnerWeb.Members.Hours"  MasterPageFile="~/Site.master" %>
<asp:Content id="PageJSContent" ContentPlaceHolderID="contentJSPlaceHolder" runat="server">
    <script src="//127.0.0.1:8082/js/partner/hours.js"></script>
</asp:Content>
<asp:Content ID="PageContent" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="section-admin-manage-business section-white-pure">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentTeamLink" href="team.aspx" class="cta admin-component-link">Team</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentPhotosLink" href="photo.aspx" class="cta admin-component-link">Photos</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentHoursLink" class="cta admin-component-link cta-underline">Hours</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="admin-component admin-component-hours active">
                        <div class="col-md-12">
                            <div class="component">
                                <button id="EditHours" class="cta cta-main cta-edit-hours">Edit Hours</button>
                                <button id="CancelHours" class="cta cta-main cta-cancel">Cancel</button>
                                <button id="SaveHours" class="cta cta-main cta-gap cta-save-hours">Save Hours</button>
                            </div>
                        </div>
                        <div class="col-md-12" ID="colxxx" runat="server">
                            <asp:Repeater ID="rpt_hours" runat="server" OnItemCreated="rpt_hours_ItemCreated">
                                <ItemTemplate>
                                    <div class="component component-v-spacing-sm component-style component-business-hours text-left">
                                        <div class="row row-flex row-flex-center">
                                            <div class="col-md-2 col-sm-12 col-xs-12">
                                                <select id="sad" runat="server" class="enabler">
                                                  <option value="1">Monday</option>
                                                  <option value="2">Tuesday</option>
                                                  <option value="3">Wednesday</option>
                                                  <option value="4">Thursday</option>
                                                  <option value="5">Friday</option>
                                                  <option value="6">Saturday</option>
                                                  <option value="0">Sunday</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2 col-sm-5 col-xs-5">
                                                <input class="text-field enabler" type="time" disabled name="" value="08:00">
                                            </div>
                                            <div class="col-md-1 col-sm-2 col-xs-2">
                                                <h6 class="enabler">to</h6>
                                            </div>
                                            <div class="col-md-2 col-sm-5 col-xs-5">
                                                <input class="text-field enabler" type="time" disabled name="" value="08:00">
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="col-md-12">
                            <button id="AddHours" class="cta cta-main cta-add-hours">+ Add Row</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>