﻿
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PartnerWeb
{
    public class User
    {
		public Guid ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string ImageURL { get; set; }
        public int PartnerID { get; set; }
        public string UserAppToken { get; set; }
        public bool EmailConfirmed { get; set; }

		private static string apiPartnerURL = "http://partnerapi.techscotch.com/api";

        public static async Task<Boolean> SignIn(string Email, string Password)
        {
            var client = new HttpClient();
			client.BaseAddress = new Uri(apiPartnerURL);

            string data = "username=" + Email + "&password=" + Password + "&grant_type=password";

            var content = new StringContent(data, Encoding.UTF8, "application/text");
            HttpResponseMessage response = await client.PostAsync("/token", content);

            // this result string should be something like: "{"token":"rgh2ghgdsfds"}"
            var result = await response.Content.ReadAsStringAsync();

            LoginToken token = JsonConvert.DeserializeObject<LoginToken>(result);
            if (!string.IsNullOrEmpty(token.access_token))
            {
				User _user = GetUserInfo(token.access_token);//get user info and save it in accountstore

                return true;
            }
            else
            {
                return false;
            }
            //return new HttpWebResponse("", "");
        }

        public static void SignUp()
        {

        }
        public static void SignOut()
        {

        }

        /// <summary>
        /// Gets the user info. 
        /// set rest to true if accountstore needs to be updated with latest
        /// </summary>
        /// <returns>The user info.</returns>
        public static User GetUserInfo(string token)
        {
            User _CustomerInfo = null;
			string url = apiPartnerURL + "/Account/UserInfo";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
			request.Headers.Add("Authorization", "bearer " + token);
            request.ContentType = "application/json";
            HttpWebResponse myResp = (HttpWebResponse)request.GetResponse();
            string responseText;

            using (var webresponse = request.GetResponse())
            {
                using (var reader = new StreamReader(webresponse.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();               
                    _CustomerInfo = JsonConvert.DeserializeObject<User>(responseText);
                }
            }
            return _CustomerInfo;

        }
    }

    public class LoginToken
    {
        public string id_token { get; set; }
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
}
