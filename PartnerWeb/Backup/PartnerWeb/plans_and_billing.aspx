﻿<%@ Page Language="C#" Inherits="PartnerWeb.plans_and_billing" MasterPageFile="~/Site.master"%>

<asp:Content ID="PageContent" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="section-admin-manage-business section-white-pure">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentPlansLink" class="cta cta-underline admin-component-link">Plans</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentBillingLink" class="cta admin-component-link">Billing</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="admin-component active admin-component-plans">
                        <div class="col-md-12">
                            <div class="component">
                                <h6>Current Plan: <span class="plan-active">Individual</span></h6>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="PlanIndividual" class="component component-v-spacing component-plan selected">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Individual</h3>
                                        <h5>For single business owners and managers</h5>
                                        <h6>Includes 1 admin/team member + unlimited video calling</h6>
                                    </div>
                                    <div class="col-md-3 text-center col-sm-12 col-xs-12">
                                        <h3 class="price">Free</h3>
                                        <h6>Forever</h6>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <button overlay-type="plan" class="cta cta-main cta-select-plan overlay-trigger">Select</button>
                                    </div>
                                </div>
                            </div>
                            <div id="PlanSmall" class="component component-v-spacing component-plan">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Small Team</h3>
                                        <h5>For teams up to 5 people</h5>
                                        <h6>Includes 1 admin & 4 team members + unlimited video calling</h6>
                                    </div>
                                    <div class="col-md-3 text-center col-sm-12 col-xs-12">
                                        <h3 class="price">$9.99/month</h3>
                                        <h6>Per user</h6>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <button overlay-type="plan" class="cta cta-main cta-select-plan overlay-trigger">Select</button>
                                    </div>
                                </div>
                            </div>
                            <div id="PlanGrowing" class="component component-v-spacing component-plan">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Growing Team</h3>
                                        <h5>For teams up to 10 people</h5>
                                        <h6>Includes 1 admin & 9 team members + unlimited video calling</h6>
                                    </div>
                                    <div class="col-md-3 text-center col-sm-12 col-xs-12">
                                        <h3 class="price">$9.99/month</h3>
                                        <h6>Per user</h6>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <button overlay-type="plan" class="cta cta-main cta-select-plan overlay-trigger">Select</button>
                                    </div>
                                </div>
                            </div>
                            <div id="PlanLarge" class="component component-v-spacing component-plan">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Large Team</h3>
                                        <h5>For teams up to 20 people</h5>
                                        <h6>Includes 1 admin & 19 team members + unlimited video calling</h6>
                                    </div>
                                    <div class="col-md-3 text-center col-sm-12 col-xs-12">
                                        <h3 class="price">$8.99/month</h3>
                                        <h6>Per user</h6>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <button overlay-type="plan" class="cta cta-main cta-select-plan overlay-trigger">Select</button>
                                    </div>
                                </div>
                            </div>
                            <div id="PlanEnterprise" class="component component-v-spacing component-plan component-plan-enterprise">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <h3>Enterprise</h3>
                                        <h5>For teams larger than 20 people</h5>
                                        <h6>Includes custom features and discounts</h6>
                                    </div>
                                    <div class="col-md-6 text-center col-sm-12 col-xs-12">
                                        <h3>Contact Us</h3>
                                        <h6>enterprise@hellojumper.com</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="admin-component admin-component-billing">
                        <div class="col-md-12">
                            <div class="component">
                                <button id="ChangeMethodButton" class="cta cta-main">Change Method</button>
                                <h6>Current Payment Method: <span class="payment-active">....1234</span></h6>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="component component-new-cc block-inputs">
                                <input class="text-field border-field" type="text" name="" value="">
                                <input class="text-field border-field" type="text" name="" value="">
                                <input class="text-field border-field" type="text" name="" value="">
                                <button id="UpdateCardButton" class="cta cta-main">Update Card</button>
                                <button class="cta cta-cancel-cc">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>