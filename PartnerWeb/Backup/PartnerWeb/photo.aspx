﻿<%@ Page Language="C#" Inherits="PartnerWeb.photo" MasterPageFile="~/Site.master"%>
<asp:Content id="PageJSContent" ContentPlaceHolderID="contentJSPlaceHolder" runat="server">
    <script src="//127.0.0.1:8082/js/partner/photo.js"></script>
</asp:Content>
<asp:Content ID="PageContent" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="section-admin-manage-business section-white-pure">
        <div class="container">
            <div class="row row-cover row-features">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentTeamLink" href="team.aspx" class="cta admin-component-link">Team</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentPhotosLink" class="cta admin-component-link cta-underline">Photos</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="component component-v-spacing text-center">
                        <a id="ComponentHoursLink" href="Members/Hours.aspx" class="cta admin-component-link">Hours</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="admin-component admin-component-photos active">
                        <div class="col-md-6">
                            <div class="component">
                                <button class="cta cta-main">Change Photo</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="component">
                                <button class="cta cta-main cta-top-spread-mobile">Change Cover</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="component component-v-spacing-sm component-style component-business-photo text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-12">
                                        <h5>Default Pic</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="business-photo-wrapper">
                                            <img class="business-photo" src="//127.0.0.1:8082/img/default-business.png"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="component component-v-spacing-sm component-style component-business-photo text-left">
                                <div class="row row-flex row-flex-center">
                                    <div class="col-md-12">
                                        <h5>Cover Pic</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="cover-photo-wrapper">
                                            <img class="cover-photo" src="//127.0.0.1:8082/img/default-cover-photo.jpg"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>